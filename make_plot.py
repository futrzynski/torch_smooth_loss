import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0,1,100)

s = 1
alpha = -1/(s*s)
beta  =  2/s
y_s1 = alpha*x*x + beta*x

s = 0.7
alpha = -1/(s*s)
beta  =  2/s
y_s08 = alpha*x*x + beta*x

fig, axes = plt.subplots(1, 2, figsize=(11,5))
axes[0].plot(x, x)
axes[0].plot(x, y_s1)
axes[0].plot(x, y_s08)
axes[0].set_aspect('equal', adjustable='box')
axes[0].grid()
axes[0].set_xlabel('Softmax of target class')
axes[0].set_ylabel('Pre-log value')
axes[0].legend(['classic crossentropy', 'soft loss, s=1', 'soft loss, s=0.7'], loc='center left', bbox_to_anchor=(1, 0.5))

start=1
axes[1].plot(x[start:], -np.log(x[start:]))
axes[1].plot(x[start:], -np.log(y_s1[start:]))
axes[1].plot(x[start:], -np.log(y_s08[start:]))
axes[1].grid()
axes[1].set_ylim([0,3])
axes[1].set_xlabel('Softmax of target class')
axes[1].set_ylabel('Loss value')
axes[1].set_aspect(0.6, adjustable='box')

plt.tight_layout()
plt.savefig('plot.png')
plt.show()