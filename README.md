# README #

This is a loss function for PyTorch which works like torch.nn.CrossEntropyLoss, but adds the following two properties:

1. The parameter `s` controls a form of label smoothing where the loss increases when the softmax output is larger than s

2. The derivative of the loss is 0 when the softmax output is equal to s. This makes this loss different from standard CrossEntropyLoss even when s=1. 

# Description

This loss transforms the softmax value of the target class using a second order polynomial in order 
If `s_c` is the sotmax output predicted for class `c`, this loss is equivalent to -log(`alpha` * `s_c`^2 + `beta` * `s_c`), where `alpha` and `beta` are determined to to satisfy the 2 properties above. 

![smooth loss plot](plot.png "smooth loss plot")


# Usage


```
loss_fn = smooth_loss(s=1)

predictions = model(input)				# predictions.shape = [batch, num_classes], from linear layer

L = loss_fn(predictions, targets)		# targets.shape = [batch], torch.long values between 0 and num_classes

print(L.item())							# prints the value of the modified loss
print(loss_fn.classic_loss)				# prints the value of the standard CrossEntropyLoss given the predictions
```