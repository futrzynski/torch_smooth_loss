class soft_loss(nn.Module):
  def __init__(self, s=1):
    """
    s: smoothed target value
    """

    super().__init__()
    # Coefficients of the polynomial used to rescale the classic softmax output
    self.alpha = -1/(s*s)
    self.beta  =  2/s

    # Value of the the usual CrossEntropyLoss for given predicitions,
    # intended for comparing convergence behavior between losses
    self.classic_loss = 0.0

  def forward(self, predicition, target):
    # Ignore negative targets, similar to torch's -100 default
    ikeep = target >= 0 
    predicition = predicition[ikeep,:]
    target = target[ikeep]

    a, _ = torch.max(predicition, dim=1, keepdim=True)
    S = torch.sum(torch.exp(predicition - a), dim=1, keepdim=False)
    a = a.flatten()
    x_c = predicition[range(predicition.size(0)), target]

    # logsumexp trick for -log(alpha*softmax^2 + beta*softmax):
    classic = torch.mean(-x_c + a + torch.log(S))
    self.classic_loss = classic.item()

    #returns torch.mean(-x_c + a + 2*torch.log(S) - torch.log(beta*S + alpha*torch.exp(x_c-a)))
    return classic + torch.mean(torch.log(S) - torch.log(self.beta*S + self.alpha*torch.exp(x_c-a)))